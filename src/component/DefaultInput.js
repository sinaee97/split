import React,{Component} from 'react'
import { StyleSheet,TextInput ,View} from "react-native";
import Icon from 'react-native-vector-icons/Ionicons'
export default class DefaultInput extends Component {
	render(){
		return(
			<View style = {{flexDirection : 'row',alignItems : 'center'}}>
				<Icon name = {this.props.iconName} size ={22} color = {this.props.valid ? '#62F63E' : '#eee'} />
				<TextInput
				{...this.props}
				maxLength = {40}
				style = {[styles.textIn,{color : this.props.valid ? '#62F63E' : '#eee'}]}
				placeholderTextColor = {'#eee'}
				/>
			</View>
		);
	}
} 

const styles = StyleSheet.create({
	textIn: {
		backgroundColor : 'transparent',
		padding: 5,
		margin : 8,
		width : '100%',
	},
})