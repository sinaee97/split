const Validate = (val,rules,conectedValue) => {
  let isValid = true
  for(let rule in rules){
    switch (rule) {
      case 'isEmail':
        isValid = isValid && emailValidator(val);
        break;
      case 'minLength' :
        isValid = isValid && passValidator(val,rules[rule]) 
        break;
      case 'equalTo' :
        isValid = isValid && passConfirmValidator(val,conectedValue)
        break;
      default:
        isValid = true
        break;
    }
  }
  return isValid;
}

const emailValidator = val => {
  return /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
  .test(val.toLowerCase());
};
const passValidator = (val,minLength) => {
  return (val.length >= minLength);
}

const passConfirmValidator = (val ,conectedValue ) => {
  return (val === conectedValue);
}
export default Validate;

