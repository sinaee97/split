import React,{ Component }  from "react";
import {View,Text,Image,TouchableOpacity,StyleSheet ,Button} from 'react-native';


export default class TopBar extends Component {
  render(){
    let str;
    if(this.props.balance < 0) str = '#f14a19'
    else str = '#13c8b5';
    return(
      <View style = {styles.container}>
        <TouchableOpacity onPress = {() => this.props.navigation.navigate('Profile')} style = {styles.touch}>
          <Image source = {require('../../assest/profile.png')} style = {styles.img} />
        </TouchableOpacity>
        <View style = {styles.vi}>
          <Text style = {[{color : str},styles.txt]} >{this.props.balance < 0 ? '' : '+'} {this.props.balance}</Text>
        </View>
        <Button title= {'ADD'} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container : {
    height : '100%' ,
    width : '100%',
    backgroundColor : '#eee' ,
    alignItems : 'center',
    flex : 1,
    flexDirection : 'row'
  },
  touch :{
    marginLeft : 10 ,
    height : 50,
    width : 50,
    marginRight : 10
  },
  img : {
    height : '100%',
    width : '100%',
  },
  vi : {
    height : '50%', 
    width : '50%',
    marginRight : 10,
    alignItems : 'center',
  },
  txt : {
    fontSize : 20,
    width : '100%',
    height : '100%',
    marginTop : '5%',
    textAlign :'center',
  }
})