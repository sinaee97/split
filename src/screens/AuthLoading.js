import React,{ Component } from "react";
import {
  AsyncStorage,
  Linking,
} from 'react-native'
import LottieView from 'lottie-react-native';


export default class AuthLoading extends Component {
  state = {userToken : null}
  constructor(props){
    super(props)
    this.getUserToken();
  }
  componentDidMount(){
    this.animation.play();
    Linking.getInitialURL().then((url) => {
      if(url){
        //adb shell am start -W -a android.intent.action.VIEW -d "temp://Auth" com.temp
        const dest = url.replace(/.*?:\/\//g, '');
        this.props.navigation.navigate(dest)
      }
    }).catch(err => console.error('An error occurred', err));
  }


  getUserToken = async () => {
    this.state.userToken = await AsyncStorage.getItem('userToken');
  }
  nxtHandeler = () => {
    this.props.navigation.navigate(this.state.userToken ? 'App' : 'Auth')
  }
  render(){
    return(
      <LottieView 
        loop = {false}
        ref={animation => {
          this.animation = animation;
        }}
        source = {require('../../assest/Main2x.json')}
        onAnimationFinish = {this.nxtHandeler}
      />
    );
  }
}