import React,{ Component }  from "react";
import {View,Text,StyleSheet} from 'react-native';

export default class Firnds extends Component {
 
  render(){
    return(
       <View style = {styles.container}>
          <Text>
            Frinds Screen
          </Text>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container : {
    height : '100%',
    width : '100%',
    backgroundColor : '#fff',
    borderWidth: 1,
    borderColor: '#ddd',
    borderTopWidth: 0,
    borderRightWidth : 0,
    borderLeftWidth : 0,
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: -10 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 4,
  }
})