import React,{ Component }  from "react";
import {View,Text,Button,StyleSheet} from 'react-native';
 
export default class Groups extends Component {

  render(){
    return(
       <View style = {styles.container}>
          <Text>
            Groups Screen
          </Text>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container : {
    height : '100%',
    width : '100%',
    backgroundColor : '#fff',
    borderWidth: 1,
    borderColor: '#ddd',
    borderTopWidth: 0,
    borderRightWidth : 0,
    borderLeftWidth : 0,
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: -10 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 4,
  }
})