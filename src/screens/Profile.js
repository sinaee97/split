import React,{ Component }  from "react";
import {View,Text,StyleSheet,Image,TouchableOpacity,CameraRoll} from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons'
export default class Profile extends Component {
  state  = {
    srcImg : null
  }
 
  takePhotoHandeler = () => {
    
  };
  render(){
    return(
      <View style = {styles.container}>
        <View style = {styles.fr}>
          <View style = {{width : '60%',height : '50%'}}>
            <Image source = { this.state.srcImg === null ? require('../../assest/profile.png') : {uri : this.state.srcImg} } style = {styles.img}/>
          </View>
          <View style= {styles.editImg}>
            <TouchableOpacity style = {styles.touch}>
              <Ionicon name = {'md-images'} size = {25} color = {'gray'}/>
              <Text style = {styles.txt}>
                  انتخاب ازگالری
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style = {styles.touch} onPress = {this.takePhotoHandeler}>
              <Ionicon name = {'md-camera'} size = {25} color = {'gray'}/>
              <Text style = {styles.txt}>
                گرفتن عکس
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style = {styles.phonNum} >
          <Text style = {[styles.txt,{fontWeight : 'bold',fontSize : 22}]}>09399999999</Text>
        </View>
        <View style = {styles.sc}>
          <TouchableOpacity style = {styles.item} onPress = {() => this.props.navigation.navigate('Auth')}>
            <Text style = {[styles.txt,{marginRight : 10}]}>
              خروج از حساب کاربری
            </Text>
          </TouchableOpacity>
          <View style = {styles.item}>
            <Text style = {[styles.txt,{marginRight : 10}]}>
              اصلاح شماره
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container : {
    width : '100%',
    height : '100%',
    backgroundColor : '#ddd'
  },
  phonNum : {
    width : '100%',height : '10%',
    justifyContent : 'center',
    alignItems :'center',
    borderBottomColor : '#eee',
    borderBottomWidth : 1,
    backgroundColor : '#eee'
  },
  fr : {
    width : '100%',height : '40%',
    justifyContent : 'center',
    alignItems :'center',
    flexDirection : "row",
    backgroundColor : '#eee'
  },
  sc : {
    width : '100%',
    height : '30%',
    marginTop : 10,
    backgroundColor : '#eee'
  },
  editImg :{
    width : '40%',
    height : '50%',
    justifyContent : 'center',
    alignItems :'center',
  },
  touch : {
    width : '90%',
    flexDirection : 'row',
    marginTop : 5,
    padding : 10,
    borderWidth : 1,
    borderColor : 'gray',
    borderRadius : 10,
    
  },
  txt : {
    fontSize : 16,
    fontWeight : '500',
    color : 'gray',
    marginLeft : 5,
  },
  img : {
    resizeMode : 'contain',
    height : '100%',
    width : '100%',
  },
  item : {
    width : '100%',
    height : '15%',
    marginTop : 10,
    borderBottomWidth : 1,
    borderBottomColor : 'gray'
  }
})