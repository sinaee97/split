import React , {Component} from 'react'
import {View,Dimensions,TouchableOpacity,ImageBackground,
        StyleSheet,Text,AsyncStorage,Animated,
        Keyboard,TouchableWithoutFeedback,Alert} from 'react-native'
import DefaultInput from '../component/DefaultInput';
import LottieView from 'lottie-react-native';
import Validate from '../component/Validate'
const window = Dimensions.get('window') 
export default class Auth extends Component{
  state = {
    control : {
      email : {
        value : '',
        valid : false,
        validationRules :{
          isEmail : true,
        }
      },
      pass :{
        value : '',
        valid : false,
        validationRules :{
          minLength : 6,
        }
      },
      passConfirm :{
        value : '',
        valid : false,
        validationRules :{
         equalTo : 'pass' 
        }
      }
    },

    isLoading : false,
    isSignIn : false
  }

  constructor(props){
    super(props);
    this._width = new Animated.Value(window.width * 0.75);
    this._wLoad = new Animated.Value(window.width * 0.6);
  }

  componentDidMount(){
  }
 
  changeHandeler = () => {
    let toVal ;
    if(this.state.isSignIn)toVal = 0.75;
    else toVal = 0;
    Animated.timing(this._width,{
      toValue : window.width * toVal,
      duration : 800
    }).start()
    this.setState({
      isSignIn : !this.state.isSignIn
    })
  }

  static navigationOptions = {
    header: null
  }
  
  
  saveUserToken = async () => {
    let str = this.state.control.email.value.toString() + this.state.control.pass.value.toString(); 
      try {
      await AsyncStorage.setItem('userToken',str)
      console.log(str)
    } catch (err) {
      console.log(err)
    }
  }
  loginHandeler= async () => {
    for(let i in this.state.control){
      if(!this.state.control[i].valid){
        if(i === 'email'){
          alert('email invalid')
          return
        }else if(i === 'pass'){
          alert('pass invalid')
          return;
        }else if(!this.state.isSignIn) {
          alert('passConfirm invalid')
          return
        }

      }
    }

    Animated.timing(this._wLoad,{
      toValue : window.width * 0.2,
      duration : 800
    }).start()
    this.setState({
      isLoading  : true,
    })
    return await
    setTimeout( () => 
      fetch('https://facebook.github.io/react-native/movies.json')
      .then(res => res.json())
      .then(res => {
        this.saveUserToken();
        this.props.navigation.navigate('App',{
          control : this.state.control
        })  
      }).catch(() => {
        Animated.timing(this._wLoad,{
          toValue : window.width * 0.6,
          duration : 800
        }).start()
        this.setState({
          isLoading  : false,
        })
        alert('Network Error')
      })
    ,1000); 
  }

  updateInput = (key,val) =>{ 
    let conectedValue = '';
    let confirmValidate = true;
    if(key === 'passConfirm'){
      conectedValue = this.state.control.pass.value;
      confirmValidate = this.state.control.pass.valid;
    }
    this.setState((prevState) => {
      return {
        control : {
          ...prevState.control,
          [key] : {
            ...prevState.control[key],
            value : val,
            valid : confirmValidate && Validate(val,prevState.control[key].validationRules,conectedValue)
          }
        }
      }
    })
  }

  render(){
    let Key
    if(this.state.isLoading){
      Key = () => {
        return(
        <Animated.View style = {[styles.signIn,{width : this._wLoad}]}>
          <TouchableOpacity disabled = {true} style = {{width : '100%',height : '100%'}}>
            <View style = {{width: '100%',height : '100%',justifyContent:'center'}}>
              <LottieView
                loop
                style = {{alignSelf : 'center',height : '100%'}}
                autoPlay
                source = {require('../../assest/_loading.json')}
              />
            </View>
          </TouchableOpacity>
        </Animated.View>
        );
      }
    }
    else {
      Key = () => {
        return(
          <Animated.View style = {[styles.signIn,{width : this._wLoad}]}>
            <TouchableOpacity onPress = {this.loginHandeler}  style = {{width : '100%',height : '100%'}}>
              <View style = {{width: '100%',height : '100%',justifyContent:'center'}}>
                <Text style = {{color:'white' ,fontSize : 20,alignSelf:'center'}}>Sign {this.state.isSignIn ? 'In' : 'Up'}</Text>
              </View>
            </TouchableOpacity>
          </Animated.View>
        );
      }
    }
    return(
      
      <TouchableWithoutFeedback onPress = {Keyboard.dismiss}>
        <ImageBackground source = {require('../../assest/login_background.jpg')} style= {{width : '100%' , height : '100%' }}>
          <View style = {styles.container} >  
            <View style = {{width : '100%' , height : '90%'}}>    
              <View style = {styles.container}>
                <View style= {{width : '100%',height:'20%'}}>
                    <Text style = {{fontSize : 25,fontWeight : 'bold',color : 'white',alignSelf:'center'}}>Welcome!</Text>
                </View>
                <View style = {styles.input}>   
                  <DefaultInput
                    iconName = 'md-person'
                    onChangeText = {(val) => this.updateInput('email',val)} 
                    value = {this.state.control.email.value}
                    placeholder = {"Please insert your email"}
                    editable = {!this.state.isLoading}
                    valid = {this.state.control.email.valid}
                  />
                  <DefaultInput
                    iconName = 'md-lock'
                    onChangeText = {(val) => this.updateInput('pass',val)} 
                    value = {this.state.control.pass.value}
                    valid = {this.state.control.pass.valid}
                    secureTextEntry = {true}
                    editable = {!this.state.isLoading}
                    placeholder = {"Password"}
                  />
                  <Animated.View style = {{width : ( this._width)}}>
                    <DefaultInput
                      iconName = 'md-checkmark'
                      secureTextEntry = {true}
                      valid = {this.state.control.passConfirm.valid}
                      editable = {!this.state.isSignIn && !this.state.isLoading }
                      onChangeText = {(val) => this.updateInput('passConfirm',val)} 
                      value = {this.state.control.passConfirm.value}
                      placeholder = {"ConfirmPassword"}
                    />
                  </Animated.View>
                </View>
                <Key/>                  
              </View> 
            </View>
            <View style = {{justifyContent : 'center',flexDirection:'row',marginBottom : 10,flex : 1,height : 10,width :'100%'}}>
                <Text style = {{fontSize : 13}}>Create a new Account? </Text> 
                <TouchableOpacity onPress = {this.changeHandeler} disabled = {this.state.isLoading}>
                  <Text style = {{fontSize : 15 ,fontWeight : 'bold'}}>Sign {this.state.isSignIn ? 'Up' : 'In'}</Text>
                </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        justifyContent: "center",
        alignItems: "center",
    },
    signIn :{
        borderRadius : 70,
        marginTop : 50,
        width : '60%',
        height : 40,
        alignItems : 'center',
        justifyContent:'center',
        backgroundColor : '#31D8DE'
    },
    input : {
        width : '75%'
    },
})

