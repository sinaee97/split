import  React from "react";
import {Text,View,StyleSheet,TouchableOpacity} from 'react-native'
import { createStackNavigator,createMaterialTopTabNavigator, createAppContainer,createSwitchNavigator ,createBottomTabNavigator } from "react-navigation";
import AuthLoading from './src/screens/AuthLoading'
import Auth from './src/screens/Auth'
import Activity from './src/screens/Activity'
import Frinds from "./src/screens/Frinds";
import Groups from "./src/screens/Groups";
import Profile from './src/screens/Profile';
import Ionicons from 'react-native-vector-icons/Ionicons';
import TopBar from "./src/component/TopBar";

const AppTabBottom = createMaterialTopTabNavigator(
  {
    Groups : Groups,
    Frinds : Frinds,
    Activity : Activity,
  },
  {
    defaultNavigationOptions : ({navigation}) => ({
      tabBarIcon : ({ focused, tintColor }) => {
        let {routeName} = navigation.state;
        let iconName ;
        let iconSize = 25;
        if(routeName === 'Frinds'){
          iconName = 'md-person';
        }else if(routeName === 'Groups'){
          iconName = 'md-people';
        }else if(routeName === 'Activity'){
          iconName = 'md-archive'
        }
        if(focused)iconSize = 30;  
        return <Ionicons name ={iconName} size = {iconSize} color = {tintColor}/>;

      },
      tabBarLabel : ({focused ,tintColor}) => {
        let {routeName} = navigation.state;
        let name='';
        if(routeName === 'Frinds'){
          name = 'دوستان';
        }else if(routeName === 'Groups'){
          name = 'گروه ها';
        }else if(routeName === 'Activity'){
          name = 'فعالیت ها'
        }
        if(!focused) name = 'a';
        return <Text style = {{color : tintColor,fontSize : 12,alignSelf :"center"}}>{name}</Text>;
      },
      
    }),
    tabBarPosition : 'bottom',
    tabBarOptions : {
      showIcon : true,
      showLabel : false,
      style : {
        backgroundColor : 'white',
        height : 50,
      },
      allowFontScaling : true,
      activeTintColor : '#13c8b5',
      inactiveTintColor : '#C9C3D1',
      indicatorStyle : {
        backgroundColor : '#13c8b5',
      }
    },
    animationEnabled : true,
    swipeEnabled : true,
  }
)
const App = createStackNavigator(
  {
    AppTabBottom : AppTabBottom,
    Profile : Profile
  },{
    defaultNavigationOptions: ({navigation}) => ({
      header : () => {
        if(navigation.state.routeName !== 'Profile'){
          return (
            <View style = {styles.headerContainer}>
              <TopBar balance = {1000} navigation = {navigation}/>
            </View>
          );
        }else{
          return (
            <View style = {{height : 50,width : '100%',backgroundColor : '#eee'}}>
            <TouchableOpacity style = {{width : '20%',height : '100%',justifyContent : 'center',marginLeft : 10}} onPress = {() => navigation.goBack()}>
              <Ionicons name = {'ios-arrow-back'} color = {'#13c8b5'} size = {30}/>
            </TouchableOpacity>
            </View>
          );
        }
      },
    })
  }
)
const TempApp = createAppContainer(createSwitchNavigator(
  {
    AuthLoading : AuthLoading,
    App : App,
    Auth : Auth
  },
  {
    initialRouteName : 'AuthLoading',
  },
  
));
export default MainApp = () => <TempApp uriPrefix = {'temp://'}/>

const styles = StyleSheet.create({
  headerContainer : {
    height : 110,
    width : '100%',
    borderWidth: 1,
    borderColor: '#ddd',
    borderTopWidth: 0,
    borderRightWidth : 0,
    borderLeftWidth : 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 10 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 4,
  },
});
